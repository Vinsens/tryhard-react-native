import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';
import React, {useEffect, useState, useMemo, Prow} from 'react';
import {useNavigation} from '@react-navigation/native';
import {Avatar, Accessory} from 'react-native-elements';

import AsyncStorage from '@react-native-community/async-storage';
import {
  Button,
  View,
  Text,
  TextInput,
  ActivityIndicator,
  StyleSheet,
  StatusBar,
  FlatList,
} from 'react-native';
import {
  Splash,
  WelcomeAuth,
  SignIn,
  SignUp,
  Home,
  Detail,
  Profile,
} from '../pages';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';

import AuthContext from '../pages/AuthContext';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();
// const AuthContext = React.createContext();

const Router = ({navigation}) => {
  const initialLoginState = {
    isLoading: true,
    username: null,
    userToken: null,
    // userLocation: null,
  };

  const loginReducer = (prevState, action) => {
    switch (action.type) {
      case 'RETRIEVE_TOKEN':
        return {
          ...prevState,
          userToken: action.token,
          isLoading: false,
        };
      case 'LOGIN':
        return {
          ...prevState,
          userName: action.id,
          userToken: action.token,
          // userLocation: action.location,
          isLoading: false,
        };
      case 'LOGOUT':
        return {
          ...prevState,
          userName: null,
          userToken: null,
          // userLocation: null,
          isLoading: false,
        };
      case 'REGISTER':
        return {
          ...prevState,
          userName: action.id,
          userToken: action.token,
          // userLocation: action.location,
          isLoading: false,
        };
    }
  };

  const [loginState, dispatch] = React.useReducer(
    loginReducer,
    initialLoginState,
  );

  const authContext = React.useMemo(() => ({
    signIn: async () => {
      useEffect(() => {
        async function fetchData() {
          let userData = null;
          await AsyncStorage.getItem('data_login').then((data) => {
            userData = JSON.parse(data);

            setUname(userData.userId);
            setEnabled(userData.enabled);
            setModif(userData.lastModified);

            console.log(userData.userId);
            console.log(userData.enabled);

            console.log(userData.lastModified);

            console.log(userData.enabled == 1);
          });

          fetchData();
        }
      }, []);

      // userToken = FoundUser[0].userToken;

      const userToken = String(FoundUser[0].userToken);

      const userName = FoundUser[0].username;
      try {
        await AsyncStorage.setItem('userToken', userToken);
      } catch (error) {
        console.log(error);
      }
      // console.log('user token', userToken);
      dispatch({type: 'LOGIN', id: userName, token: userToken});
    },
    signOut: async () => {
      try {
        await AsyncStorage.removeItem('userToken');
      } catch (error) {
        console.log(error);
      }

      dispatch({type: 'LOGOUT'});
    },
    signUp: () => {
      setUserToken('asdf');
      setIsLoading(false);
    },
  }));

  useEffect(() => {
    setTimeout(async () => {
      let userToken;
      userToken = null;
      try {
        userToken = await AsyncStorage.getItem('userToken', userToken);
      } catch (error) {
        console.log(error);
      }
      // console.log('user token', userToken);
      dispatch({type: 'RETRIEVE_TOKEN', token: userToken});
    }, 1000);
  }, []);

  if (loginState.isLoading) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator size="large" color="#009387" />
      </View>
    );
  }
  return (
    <AuthContext.Provider value={authContext}>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="WelcomeAuth" component={WelcomeAuth} />
        <Stack.Screen name="SignIn" component={SignIn} />
        <Stack.Screen name="SignUp" component={SignUp} />
        <Stack.Screen name="Splash" component={Splash} />

        <Stack.Screen name="Home" component={DrawerScreen} />
      </Stack.Navigator>
    </AuthContext.Provider>
  );
};

function CustomDrawerContent(props) {
  const navigation = useNavigation();
  const [isLoading, setLoading] = useState(true);
  const [uname, setUname] = useState([]);
  const [enabled, setEnabled] = useState([]);
  const [modif, setModif] = useState([]);
  const [login, setLogin] = useState([]);

  useEffect(() => {
    async function fetchData() {
      let userData = null;
      const response = await AsyncStorage.getItem('data_login').then((data) => {
        userData = JSON.parse(data);
        // console.log(userData);
        setUname(userData.userId);
        setEnabled(userData.enabled);
        setModif(userData.lastModified);

        if ((userData.enabled = !1)) {
          navigation.navigate('SignIn');
        }
      });
      // ...
    }
    fetchData();
  }, []);

  return (
    <View style={{flex: 1}}>
      <StatusBar backgroundColor="#08d4c4" barStyle="light-content" />
      <DrawerContentScrollView {...props}>
        <View style={styles.drawerContent}>
          <View style={styles.userInfoSection}>
            <View
              style={{flexDirection: 'row', marginTop: 15, paddingLeft: 20}}>
              {/* <Text>{uname}</Text> */}
              <Avatar
                rounded
                source={{
                  uri:
                    'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
                }}
                size={50}
              />
              <Text>{uname}</Text>
            </View>
          </View>

          <View style={styles.drawerSection}>
            <DrawerItem
              labelStyle={{fontSize: 10}}
              icon={({focused, color, size}) => (
                <Icon size={25} name={'info-circle'} />
              )}
              label="Detail"
              onPress={() => {
                props.navigation.navigate('Detail');
              }}
            />
            <DrawerItem
              labelStyle={{fontSize: 10}}
              icon={({focused, color, size}) => (
                <Icon size={25} name={'user'} />
              )}
              label="Profile"
              onPress={() => {
                props.navigation.navigate('Profile');
              }}
            />
          </View>
        </View>
      </DrawerContentScrollView>

      <View style={styles.bottomDrawerSection}>
        <DrawerItem
          label="Logout"
          labelStyle={{fontSize: 10, fontWeight: 'bold'}}
          icon={({focused, color, size}) => (
            <Icon size={20} name={'sign-out'} />
          )}
          onPress={() => {
            AsyncStorage.clear();
            navigation.navigate('SignIn', alert('Logout successfull!!'));
          }}
        />
      </View>
    </View>
  );
}

const DrawerScreen = () => {
  return (
    <Drawer.Navigator
      drawerContent={(props) => <CustomDrawerContent {...props} />}
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="DrawerHome">
      <Drawer.Screen name="DrawerHome" component={MyTabs} />
    </Drawer.Navigator>
  );
};

const MyTabs = () => (
  <Tab.Navigator
    screenOptions={({route}) => ({
      tabBarIcon: ({focused, color, size}) => {
        let iconName;

        if (route.name === 'Home') {
          iconName = focused ? 'archive' : 'archive';
        } else if (route.name === 'Detail') {
          iconName = focused ? 'adjust' : 'adjust';
        } else if (route.name === 'Profile') {
          iconName = focused ? 'anchor' : 'anchor';
        }
        // You can return any component that you like here!
        return <Icon name={iconName} size={size} color={color} />;
      },
    })}
    tabBarOptions={{
      activeTintColor: '#009387',
      inactiveTintColor: 'black',
    }}>
    <Tab.Screen name="Home" component={Home} />
    <Tab.Screen name="Detail" component={Detail} />

    <Tab.Screen name="Profile" component={Profile} />
    {/* <Tab.Screen name="Details" component={DetailStackScreen} />
    <Tab.Screen name="Profile" component={ProfileStackScreen} /> */}
  </Tab.Navigator>
);

export default Router;

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },

  userInfoSection: {
    paddingTop: 10,
  },

  title: {
    fontSize: 16,
    marginTop: 3,
    fontWeight: 'bold',
  },

  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  row: {
    marginTop: 20,
    marginLeft: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 15,
  },
  paragraph: {
    fontWeight: 'bold',
    marginRight: 3,
  },
  drawerSection: {
    marginTop: 15,
  },

  bottomDrawerSection: {
    marginBottom: 15,
    borderTopColor: '#f4f4f4',
    borderTopWidth: 1,
  },
  preference: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});
