// import * as React from 'react';
import React, {useEffect, useState} from 'react';
import {
  Button,
  View,
  Text,
  TextInput,
  FlatList,
  SafeAreaView,
  Card,
  StyleSheet,
  ScrollView,
  StatusBar,
  KeyboardAvoidingView,
  Picker,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Header, Avatar, Input, Overlay} from 'react-native-elements';

import Icon from 'react-native-vector-icons/FontAwesome';
import {useNavigation} from '@react-navigation/native';

import AsyncStorage from '@react-native-community/async-storage';
const Home = ({navigation}) => {
  const [uname, setUname] = useState([]);
  const [enabled, setEnabled] = useState([]);
  const [modif, setModif] = useState([]);
  const [login, setLogin] = useState([]);

  useEffect(() => {
    async function fetchData() {
      let userData = null;
      await AsyncStorage.getItem('data_login').then((data) => {
        userData = JSON.parse(data);

        setUname(userData.userId);
        setEnabled(userData.enabled);
        setModif(userData.lastModified);

        console.log(userData.userId);
        console.log(userData.enabled);

        console.log(userData.lastModified);

        console.log(userData.enabled == 1);
      });

      fetchData();
    }
  }, []);
  return (
    <KeyboardAvoidingView>
      <ScrollView>
        <SafeAreaView>
          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <Text>{uname}</Text>
            <Text>{enabled}</Text>
            <Text>{modif}</Text>
            <Text>{login}</Text>
            {/* <StatusBar backgroundColor="blue" barStyle="light-content" /> */}
            <Header
              style={styles.Header}
              leftComponent={{
                icon: 'menu',
                color: '#fff',
                onPress: () => navigation.toggleDrawer(),
              }}
              centerComponent={{style: {color: '#fff'}}}
              rightComponent={{icon: 'home', color: '#fff'}}
              containerStyle={{
                backgroundColor: '#08d4c4',
                justifyContent: 'space-around',
              }}
            />
            <Button
              title="Test Detail"
              onPress={() => navigation.navigate('Detail')}
            />

            <Button
              title="Test Profile"
              onPress={() => navigation.navigate('Profile')}
            />
            <Text> INI HOME SCREEN!</Text>
          </View>
        </SafeAreaView>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    paddingTop: 100,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  TitleSection: {
    marginTop: 20,
  },
  Card: {
    minHeight: 220,
    minWidth: 200,
  },

  Header: {
    backgroundColor: '#08d4c4',
  },
});
