import React, {useEffect, useState, useMemo, Prow} from 'react';
import {
  Button,
  View,
  Text,
  TextInput,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Platform,
  StatusBar,
  Alert,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import Icon from 'react-native-vector-icons/MaterialIcons';

import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Feather from 'react-native-vector-icons/Feather';

import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import AsyncStorage from '@react-native-community/async-storage';

import AuthContext from '../AuthContext';

const SignIn = ({navigation}) => {
  const [data, setData] = React.useState({
    username: '',
    password: '',
    success: '',
    enabled: '',
    login: 0,
    check_textInputChange: false,
    secureTextEntry: true,
    isValidUser: true,
    isValidPassword: true,
  });
  const {signIn} = React.useContext(AuthContext);

  const textInputChange = (val) => {
    if (val.trim().length >= 4) {
      setData({
        ...data,
        username: val,
        check_textInputChange: true,
        isValidUser: true,
      });
    } else {
      setData({
        ...data,
        username: val,
        check_textInputChange: false,
        isValidUser: false,
      });
    }
  };

  const textInputChangePass = (val) => {
    if (val.trim().length >= 8) {
      setData({
        ...data,
        password: val,
        isValidPassword: true,
      });
    } else {
      setData({
        ...data,
        password: val,
        isValidPassword: false,
      });
    }
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry,
    });
  };

  const handleValidUser = (val) => {
    if (val.trim().length > 4) {
      setData({
        ...data,
        isValidUser: true,
      });
    } else {
      setData({
        ...data,
        isValidUser: false,
      });
    }
  };

  const loginHandle = async (username, password) => {
    try {
      if (data.username.length == 0 || data.password.length == 0) {
        Alert.alert(
          'Wrong input! ',
          'Username or password field cannot be empty.',
          [{text: 'Okay'}],
        );
        return;
      }
      console.log(username);
      console.log(password);

      fetch('http://150.242.110.240:8280/loginAppBonusTpay', {
        method: 'POST',
        headers: {
          Accept: 'application/json, text/plain',
          'Content-Type': 'application/json;charset=UTF-8',
        },
        body: JSON.stringify({
          userID: username,
          password: password,
        }),
      })
        .then((response) => response.json())
        .then(async (data) => {
          if (data.success == true && data.data[0].enabled == 1) {
            await AsyncStorage.setItem(
              'data_login',
              JSON.stringify(data.data[0]),
            );

            navigation.navigate('Home');
          } else {
            Alert.alert('Username or password incorrect!');
            navigation.navigate('SignIn');
          }
        })

        .catch((error) => console.log('Error detected: ' + error));
    } catch (error) {
      console.log(error);
    }
  };

  const [uname, setUname] = useState([]);
  const [enabled, setEnabled] = useState([]);
  const [modif, setModif] = useState([]);
  const [login, setLogin] = useState([]);

  useEffect(() => {
    async function fetchData() {
      let userData = null;
      await AsyncStorage.getItem('data_login').then((data) => {
        userData = JSON.parse(data);

        setUname(userData.userId);
        setEnabled(userData.enabled);
        setModif(userData.lastModified);

        console.log(userData.enabled == 1);

        // if (userData.enabled == 1) {
        //   navigation.navigate('Home');
        // } else if (userData.enabled != 1) {
        //   navigation.navigate('SignIn');
        // } else {
        // }
      });
      // const response = await MyAPI.getData(someId);
      // ...
    }
    fetchData();
  }, []);

  return (
    <View style={styles.container}>
      <View></View>
      <StatusBar backgroundColor="#009387" barStyle="light-content" />
      <View style={styles.header}>
        <Text style={styles.text_header}>Welcome !</Text>
      </View>
      {/* {/* <Text style={{paddingTop: 30, color: 'red'}}>{enabled}</Text> */}
      {/* <Text style={{paddingTop: 30, color: 'red'}}>{uname}</Text> */}
      {/* <TouchableOpacity>
        <Text> {enabled} </Text>
      </TouchableOpacity> */}
      <Animatable.View animation="fadeInUpBig" style={styles.footer}>
        <Text style={styles.text_footer}>username</Text>
        <View style={styles.action}>
          <FontAwesome name="user-o" color="#05375a" size={20} />
          <TextInput
            placeholder="Your username"
            style={styles.TextInput}
            onChangeText={(val) => textInputChange(val)}
            onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
          />
          {data.check_textInputChange ? (
            <Animatable.View animation="bounceIn">
              <Feather name="check-circle" color="green" size={15} />
            </Animatable.View>
          ) : null}
        </View>
        {data.isValidUser ? null : (
          <Animatable.View animation="fadeInLeft" duration={500}>
            <Text>Username must be 4 characters long.</Text>
          </Animatable.View>
        )}
        <Text style={styles.text_footer}>Password</Text>
        <View style={styles.action}>
          <FontAwesome name="lock" color="#05375a" size={20} />
          <TextInput
            placeholder="Your password"
            secureTextEntry={data.secureTextEntry ? true : false}
            style={styles.TextInput}
            onChangeText={(val) => textInputChangePass(val)}
          />

          <TouchableOpacity onPress={updateSecureTextEntry}>
            {data.secureTextEntry ? (
              <Feather name="eye-off" color="green" size={15} />
            ) : (
              <Feather name="eye" color="green" size={15} />
            )}
          </TouchableOpacity>
        </View>
        {data.isValidPassword ? null : (
          <Animatable.View animation="fadeInLeft" duration={500}>
            <Text>Password must be 8 characters long.</Text>
          </Animatable.View>
        )}
        <View style={styles.button}>
          <TouchableOpacity
            onPress={() => loginHandle(data.username, data.password)}
            style={styles.signIn}>
            <LinearGradient
              colors={['#08d4c4', '#01ab9d']}
              style={styles.signIn}>
              <Text style={(styles.textSign, {color: '#fff'})}>Sign In</Text>
            </LinearGradient>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => navigation.navigate('SignUp')}
            style={[
              styles.signIn,
              {
                borderColor: '#009387',
                borderWidth: 1,
                marginTop: 15,
              },
            ]}>
            <Text
              style={
                (styles.textSign,
                {
                  color: '#009387',
                })
              }>
              Sign Up
            </Text>
          </TouchableOpacity>
        </View>
      </Animatable.View>
    </View>
  );
};

export default SignIn;

const {height} = Dimensions.get('screen');
const height_logo = height * 0.28;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f8ff',
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5,
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingBottom: 50,
  },
  footer: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 30,
    paddingHorizontal: 20,
  },
  logo: {
    width: height_logo,
    height: height_logo,
  },

  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },

  title: {
    color: '#05375a',
    fontSize: 30,
    fontWeight: 'bold',
  },
  text: {
    color: 'grey',
    marginTop: 5,
  },

  text_header: {
    paddingLeft: 10,
    color: 'black',
    fontWeight: 'bold',
    fontSize: 30,
  },

  text_footer: {
    color: '#05375a',
    fontSize: 18,
  },

  button: {
    alignItems: 'center',
    marginTop: 50,
  },

  signIn: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },

  TextInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },

  textSign: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});

{
  /* export default function SignIn({navigation}) { */
}
{
  /* //   return ( */
}
{
  /* //     <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}> */
}
{
  /* //       <Text>Sign In Screen</Text> */
}

{
  /* //       <Button */
}
{
  /* //         title="Go to Home" */
}
{
  /* //         onPress={() => navigation.navigate('HomeScreen')} */
}
{
  /* //       /> */
}
{
  /* //       <Button title="Go back" onPress={() => navigation.goBack()} /> */
}
{
  /* //     </View> */
}
{
  /* //   ); */
}
{
  /* // } */
}
