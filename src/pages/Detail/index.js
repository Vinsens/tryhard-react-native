// import * as React from 'react';
import React, {useEffect, useState} from 'react';
import {
  Button,
  View,
  Text,
  TextInput,
  FlatList,
  SafeAreaView,
  Card,
  StyleSheet,
  ScrollView,
  StatusBar,
  KeyboardAvoidingView,
  Picker,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Header, Avatar, Input, Overlay} from 'react-native-elements';

import Icon from 'react-native-vector-icons/FontAwesome';
import {useNavigation} from '@react-navigation/native';

const Detail = ({navigation}) => {
  return (
    <KeyboardAvoidingView>
      <ScrollView>
        <SafeAreaView>
          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <Header
              style={styles.Header}
              leftComponent={{
                icon: 'menu',
                color: '#fff',
                onPress: () => navigation.toggleDrawer(),
              }}
              centerComponent={{style: {color: '#fff'}}}
              rightComponent={{icon: 'home', color: '#fff'}}
              containerStyle={{
                backgroundColor: '#08d4c4',
                justifyContent: 'space-around',
              }}
            />

            <Button title="Test alert" onPress={() => alert('testing alert')} />
            <Text> INI DETAIL SCREEN!</Text>
          </View>
        </SafeAreaView>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

// function Detail({navigation}) {
//   const [isLoading, setLoading] = useState(true);

//   return (
//     <KeyboardAvoidingView>
//       <ScrollView>
//         <SafeAreaView>
//           <View style={{alignItems: 'center', justifyContent: 'center'}}>
//             <Header
//               style={styles.Header}
//               leftComponent={{
//                 icon: 'menu',
//                 color: '#fff',
//                 onPress: () => navigation.toggleDrawer(),
//               }}
//               centerComponent={{style: {color: '#fff'}}}
//               rightComponent={{icon: 'home', color: '#fff'}}
//               containerStyle={{
//                 backgroundColor: '#08d4c4',
//                 justifyContent: 'space-around',
//               }}
//             />

//             <Button title="Test alert" onPress={() => alert('testing alert')} />
//             <Text> INI DETAIL SCREEN!</Text>
//           </View>
//         </SafeAreaView>
//       </ScrollView>
//     </KeyboardAvoidingView>
//   );
// }

export default Detail;

const styles = StyleSheet.create({
  container: {
    paddingTop: 100,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  TitleSection: {
    marginTop: 20,
  },
  Card: {
    minHeight: 220,
    minWidth: 200,
  },

  Header: {
    backgroundColor: '#08d4c4',
  },
});
