export const colors = {
  default: '#f8f8ff',
  disable: 'black',
  dark: 'black',
  white: '#fff',
};
